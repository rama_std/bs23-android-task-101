# About

A simple Android application that shows the most starred GitHub repositories by searching with any keyword.

#### Install and test latest version by link below 👇

##### Download link from Google Drive [https://drive.google.com/file/d/1GPZgUM_nxMzWTLjhQ7jKr8f03VHYTcvt/view?usp=sharing].

### App Screenshots

![image_screen](/uploads/6709129ad8322dc0761289ce7b328069/image_screen.PNG)


### Architecture: MVVM (Model View View-Model)

![mvvm2](/uploads/9b2e1bc637db7b7478aca9c5c93d2ec0/mvvm2.png)

### Navigation Graph

![navigation_ui](/uploads/944fb1b13ec7c5bdaa4dc9d325c7999c/navigation_ui.PNG)

### Project Structure

![project-structure](/uploads/ae05d0429479caadef9db6a7e38f3b81/project-structure.PNG)


