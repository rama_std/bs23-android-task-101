package com.ramananda.githubsearchbs23.application

import DetailsViewModelFactory
import SearchViewModelFactory
import android.app.Application
import com.ramananda.githubsearchbs23.data.database.AppDatabase
import com.ramananda.githubsearchbs23.data.network.GithubApis
import com.ramananda.githubsearchbs23.data.preference.PreferenceProvider
import com.ramananda.githubsearchbs23.data.repository.DetailsRepository
import com.ramananda.githubsearchbs23.data.repository.SearchRepository
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class App : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@App))
        bind() from singleton { GithubApis() }
        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { PreferenceProvider(instance()) }
        bind() from singleton { SearchRepository(instance(), instance(), instance(), instance()) }
        bind() from singleton { DetailsRepository(instance(), instance(), instance()) }
        bind() from provider { SearchViewModelFactory(instance()) }
        bind() from provider { DetailsViewModelFactory(instance()) }

    }

}