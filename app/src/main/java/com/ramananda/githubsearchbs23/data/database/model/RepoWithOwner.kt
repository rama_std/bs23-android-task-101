package com.ramananda.githubsearchbs23.data.database.model
import androidx.room.Embedded

data class RepoWithOwner(
    @Embedded val repo: Repo,
    @Embedded val owner: Owner
)