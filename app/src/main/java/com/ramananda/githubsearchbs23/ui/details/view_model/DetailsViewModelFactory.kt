import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ramananda.githubsearchbs23.data.repository.DetailsRepository
import com.ramananda.githubsearchbs23.ui.details.view_model.DetailsViewModel

@Suppress("UNCHECKED_CAST")
class DetailsViewModelFactory(
    private val repository: DetailsRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return DetailsViewModel(repository) as T
    }
}