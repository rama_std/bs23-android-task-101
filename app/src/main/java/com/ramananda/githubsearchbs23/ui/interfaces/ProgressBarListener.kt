/** Allows to show and hide activity's progressbar from fragments in navigation graph */
interface ProgressBarListener {

    interface Activity {
        fun show()
        fun hide()

    }

    interface Fragment {
        fun showProgressBar()
        fun hideProgressBar()

    }
}