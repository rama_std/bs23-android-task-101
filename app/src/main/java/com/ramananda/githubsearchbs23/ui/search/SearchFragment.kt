package com.ramananda.githubsearchbs23.ui.search

import ProgressBarListener
import SearchViewModel
import SearchViewModelFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.koshsu.githubsearch.utils.toastS
import com.ramananda.githubsearchbs23.R
import com.ramananda.githubsearchbs23.data.Status
import com.ramananda.githubsearchbs23.ui.interfaces.SearchListener
import com.ramananda.githubsearchbs23.ui.search.adapter.SearchAdapter
import kotlinx.android.synthetic.main.fragment_search.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class SearchFragment : Fragment(), ProgressBarListener.Fragment, KodeinAware,
    SearchListener.Fragment {

    // DI
    override val kodein by kodein();
    private val factory: SearchViewModelFactory by instance()

    private lateinit var viewModel: SearchViewModel
    private lateinit var searchQuery: String
    private lateinit var searchAdapter: SearchAdapter


    private val args: SearchFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchQuery = args.searchQuery

        (requireActivity() as SearchListener.Activity).registerSearchFragment(this)
        (requireActivity() as SearchListener.Activity).setSearchText(searchQuery)

        // if you want to avoid unnecessary re-fetching data put this initialization here
        viewModel = ViewModelProvider(this, factory).get(SearchViewModel::class.java)
        getData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bindUI()
        initReposObserver()
    }

    // Observe data & update UI
    private fun initReposObserver() {

        viewModel.repos.observe(viewLifecycleOwner, Observer { status ->
            when (status) {
                is Status.Loading -> showProgressBar()
                is Status.Success -> {
                    hideProgressBar()
                    if (status.data.isNotEmpty() && status.data != searchAdapter.repos) {
                        searchAdapter.apply {
                            this.repos = status.data
                        }
                    }
                }
                is Status.Error -> {
                    requireContext().toastS(status.errorMessage)
                    hideProgressBar()
                }
            }
        })
    }

    // Request data by search
    private fun getData() = viewModel.getRepos(searchQuery)

    // Show or hide activity's progressbar
    override fun showProgressBar() = (requireActivity() as ProgressBarListener.Activity).show()
    override fun hideProgressBar() = (requireActivity() as ProgressBarListener.Activity).hide()

    // casual ui binds
    private fun bindUI() {
        searchAdapter = SearchAdapter()
        recycler_view.apply {
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
            adapter = searchAdapter
        }
    }

    // do search from activity > toolbar > SearchView
    override fun doSearch(searchQuery: String) {
        this.searchQuery = searchQuery
        getData()
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as SearchListener.Activity).showSearchView(true)
    }

}