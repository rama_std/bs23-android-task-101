import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ramananda.githubsearchbs23.data.repository.DetailsRepository
import com.ramananda.githubsearchbs23.data.repository.SearchRepository
import com.ramananda.githubsearchbs23.ui.details.view_model.DetailsViewModel

/**
 * This class is needed for injecting repository into the ViewModel by DI
 * */
@Suppress("UNCHECKED_CAST")
class SearchViewModelFactory(
    private val repository: SearchRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SearchViewModel(repository) as T
    }
}