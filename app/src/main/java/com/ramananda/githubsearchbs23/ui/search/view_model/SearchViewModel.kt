import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ramananda.githubsearchbs23.data.Status
import com.ramananda.githubsearchbs23.data.database.model.Repo
import com.ramananda.githubsearchbs23.data.repository.SearchRepository
import kotlinx.coroutines.launch

class SearchViewModel(
    private val repository: SearchRepository
) : ViewModel() {

    private val _repos = MutableLiveData<Status<List<Repo>>>()
    val repos: LiveData<Status<List<Repo>>> get() = _repos

    fun getRepos(searchQuery: String) {
        viewModelScope.launch {
            repository.getRepos(searchQuery).collect {
                _repos.value = it
            }
        }
    }

}