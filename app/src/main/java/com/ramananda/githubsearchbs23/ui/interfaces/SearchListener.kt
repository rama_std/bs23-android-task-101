package com.ramananda.githubsearchbs23.ui.interfaces

/** Allows to manipulate activity's SearchView from child fragments in nav_graph.xml */
interface SearchListener {

    interface Activity {
        fun showSearchView(isShown: Boolean)
        fun setSearchText(searchQuery: String)
        fun registerSearchFragment(instance: Fragment)

    }

    interface Fragment {
        fun doSearch(searchQuery: String)
    }
}